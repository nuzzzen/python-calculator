# A simple python calculator

#### version 1.0

## Supported operations:

- Sum
- Multiplication
- Subtraction
- Division
- Square Root

## TODO:

- Better number input
- Definite Integrals
- Derivatives
